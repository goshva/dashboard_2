'use strict'

module.exports = {
  NODE_ENV: '"production"',
  SERVER_URL: process.env.SERVER_URL,
  APP_ID: process.env.APP_ID,
  SENTRY_DSN: process.env.SENTRY_DSN,
  FB_APP_ID: process.env.FB_APP_ID,
  STRIPE_APP_ID: process.env.STRIPE_APP_ID
}
