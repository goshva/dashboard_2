import budget_allocation from '@svg/budget_allocation.svg?inline';
import narrow_interests from '@svg/narrow_interests.svg?inline';
import pab from '@svg/pab.svg?inline';
import bt from '@svg/bt.svg?inline';
import narrow_age_gender from '@svg/narrow_age_gender.svg?inline';
import narrow_placement from '@svg/narrow_placement.svg?inline';
import lal from '@svg/lal.svg?inline';
import AssistantIcon from '@svg/assistant.svg?inline';
import NotificationIcon from '@svg/notification.svg?inline';
import EditIcon from '@svg/edit.svg?inline';
import PencilIcon from '@svg/pencil.svg?inline';
import TurnOffIcon from '@svg/turn-off.svg?inline';
import CopyIcon from '@svg/copy.svg?inline';
import TrashIcon from '@svg/trash.svg?inline';

export default {
    budget_allocation,
    bt,
    pab,
    lal,
    narrow_age_gender,
    narrow_placement,
    narrow_interests,
    AssistantIcon,
    NotificationIcon,
    EditIcon,
    PencilIcon,
    TurnOffIcon,
    TrashIcon,
    CopyIcon
};
