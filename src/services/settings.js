import parseQuery from '@/utils/parse-query';

export function fetchLabels () {
    return parseQuery('AppExtras').find();
}
