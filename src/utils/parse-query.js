import Parse from 'parse';

function parseQuery (className) {
    const parseObject = Parse.Object.extend(className);
    return new Parse.Query(parseObject);
}

export default parseQuery;
