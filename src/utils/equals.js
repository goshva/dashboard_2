import isObject from '@/utils/is-object';
import Parse from 'parse';

function isParseObject (object) {
    return object instanceof Parse.Object;
}

export default function equals (a, b) {
    if (a === b) {
        return true;
    }

    const isObjectA = isObject(a);
    const isObjectB = isObject(b);

    if (isObjectA && isObjectB) {
        try {
            const isArrayA = Array.isArray(a);
            const isArrayB = Array.isArray(b);
            if (isArrayA && isArrayB) {
                return a.length === b.length && a.every((e, i) => equals(e, b[i]));
            } else if (!isArrayA && !isArrayB) {
                if (isParseObject(a) && isParseObject(b)) {
                    return a.equals(b);
                }

                const keysA = Object.keys(a);
                const keysB = Object.keys(b);

                return keysA.length === keysB.length &&
                    keysA.every(key => equals(a[key], b[key]));
            }
        } catch (e) {
            return false;
        }
    } else if (!isObjectA && !isObjectB) {
        return String(a) === String(b);
    } else {
        return false;
    }

    return false;
}
