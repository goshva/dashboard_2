import store from '@/store';
import router from '@/router';
import { setCookie , getCookie } from 'tiny-cookie';

/* eslint-disable no-undef, no-process-env */
const { FB_APP_ID } = process.env;
const fbSdk = {};

function install(Vue) {
    window.fbAsyncInit = () => {

        FB.init({
            appId: FB_APP_ID,
            autoLogAppEvents: true,
            cookie: true,
            xfbml: true,
            oauth: true,
            version: 'v3.2'
        });
        FB.Event.subscribe('auth.statusChange', function(response) {
            if (response.authResponse) {
                const user = {
                    id: response.authResponse.userID,
                    loggedIn: true,
                    access_token: response.authResponse.accessToken,
                };
        fetch(`/api/user/${user.id}/exchange_token?access_token=${user.access_token}`, {
            method: "get",
            headers: {
                'Authorization': `Bearer ${user.access_token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then(function(response) {
            return response.json();
        })
        .then(function(api) {
            user.access_token = api.access_token;
              setCookie('apiToken',user.access_token,{ domain: '.leadza.ai' });
              setCookie('fbid',user.id, { domain: '.leadza.ai' });
            if (router.history.current.path === '/login') {
                router.go('-1');
            }

        });

            } else {
                console.log(' user has not auth\'d your app, or is not logged into Facebook');
            }
        });
        FB.Event.subscribe('auth.logout', function(response) {
            if (response) {
                router.go(-1);
            } else {
                alert('error logout');
            }
        });
        Vue.prototype.FB = FB;
    };

    (((d, s, id) => {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
        //js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk'));
    /* eslint-enable */
}

fbSdk.install = install;
export default fbSdk;
