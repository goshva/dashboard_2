import axios from 'axios';
import store from '@/store';
import { isCookieEnabled, getCookie, setCookie, removeCookie } from 'tiny-cookie';
const state = {
    apiSettings: {accounts_and_campaigns:{}},
    apiTipconfigs: {}
};
const actions = {
    async getTipconfigs (context) {
            axios
                .get(`/api/user/${store.getters['auth/userId']}/tipconfigs`)
                .then(response => {
                    context.commit('setTipconfigs', response.data);
                    //store.dispatch('auth/LogIn',user);
                    // context.$emit('createUser', user);
                })
                .catch(error => {
                    console.log('Error get settings');
                    console.log(error);
                    return true;
                })
    },
    async getSettings (context) {
            axios
                .get(`/api/user/${store.getters['auth/userId']}/settings`)
                .then(response => {
                    context.commit('setSettings', response.data);
                    //store.dispatch('auth/LogIn',user);
                    // context.$emit('createUser', user);
                })
                .catch(error => {
                    console.log('Error get settings');
                    console.log(error);
                    return true;
                })
                .finally(() => (this.loading = false));
    }
};

const mutations = {
    setSettings (state, data) {
        state.apiSettings = data;
    },
    setTipconfigs (state, data) {
        state.apiTipconfigs = data;
    }
};

const getters = {
    acc_and_cam: state => state.apiSettings.accounts_and_campaigns,
    accs: state => state.apiSettings.accounts_and_campaigns.accounts,
    tipconfigs: state => state.apiTipconfigs.tipconfigs,
//    loggedIn: state => Boolean(state.currentUser.loggedIn),
};
export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};
