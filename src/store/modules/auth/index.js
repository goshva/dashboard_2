import axios from 'axios';
import store from '@/store';
import { isCookieEnabled, getCookie, setCookie, removeCookie } from 'tiny-cookie';
const state = {
    currentUser: {
        loggedIn:false,
        access_token:false
    }
};
const actions = {
    async LogIn (context,user) {
       context.commit('setCurrentUser', user);
    },
    async logOut (context) {
        context.commit('setCurrentUser', {access_token:null, loggedIn:false });
        removeCookie('apiToken', { domain: '.leadza.ai' });
        removeCookie('fbid', { domain: '.leadza.ai' });
        window.FB.logout();
        window.location = "/"
    },
    async CreatesNewUser (context) {

        window.FB.api(`/me?fields=first_name,last_name,email&access_token=${state.currentUser.access_token}`, function(response) {
            const user = response;
            if (!user.hasOwnProperty('email')  && !user.hasOwnProperty('error')){
                user.email = prompt('Enter your e-mail');
                if ( user.email == null ||  user.email == "" ) {
                      store.dispatch('auth/LogOut');
                }
            }; 
            user.first_name = response.first_name
            user.last_name = response.last_name
            user.access_token  = state.currentUser.access_token
            if (user.hasOwnProperty('error')){
                      alert(user.error)
                      store.dispatch('auth/LogOut');
            }
            axios
                .put(`/api/user/${store.getters['auth/userId']}`, user)
                .then(response => {
                    user.exist  = true;
                    context.commit('setCurrentUser', user);
                    store.dispatch('auth/LogIn',user);
                    // context.$emit('createUser', user);
                })
                .catch(error => {
                    console.log('Error create user');
                    console.log(error);
                    store.dispatch('auth/LogOut');
                    return true;
                })
                .finally(() => (this.loading = false));
        });
    }
};

const mutations = {
    setCurrentUser (state, user) {
        state.currentUser = user;
        /* eslint-disable camelcase */
        axios.defaults.headers.common.Authorization = `Bearer ${state.currentUser.access_token}`;
		axios.interceptors.response.use((response) => {
			return response
		  }, (error) => {
			if (error.response.status == 403) {
			//   store.dispatch('auth/logOut');
			}
			return Promise.reject(error)
		  })
        /* eslint-enable camelcase */
    }
};

const getters = {
    user: state => state.currentUser,
    loggedIn: state => Boolean(state.currentUser.loggedIn),
    userName: state => state.currentUser.name,
    userId: state => state.currentUser.id,
    email: state => state.currentUser.email,
    botStatus: state => state.currentUser.botstatus,
    iso: state => state.currentUser.iso,
    exist: state => Boolean(state.currentUser.exist),
    existUser: state => state.currentUser.exist
};
export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};
